# Trees and Graphs: Binary Search Tree

<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

An insertion operation can be handled by solving two sub-problems:<br>
&emsp;\- finding the proper place (search)<br>
&emsp;\- inserting the new node into that place (append)

Binary search tree (BST) rules can help you decide where to place a new node (y).<br>
Let x be a node in a binary search tree:<br>
&emsp;\- If  y < x, then y should be a node in the left subtree of x<br>
&emsp;\- If  y > x, then y should be a node in the right subtree of x

<b>`Hint:`&nbsp;Starting from the root node, traverse through the tree.  Stop at every node and check if one of its subtrees:<br>
&emsp;\- is empty; and<br>
&emsp;\- is the correct place (according to the BST rules)</b>

 If so, assign the new node into that place.
 
 The illustration below shows the algorithm flow. Starting with the root node, it iterates through the tree and checks the BST rules until it finds the empty location.
 
 <img src="images/trees_and_graphs/binary-search-tree/binary-search-tree-1.png"  width="600"><br>
 
 <b>`Note:`</b>&nbsp;Keep in mind that the entire tree can be empty.<br>
 
 <b>`Note:`</b>&nbsp;Finding a spot in a tree is very similar to finding an element or an index in a linked list. You can use the pointer objects for the iteration (the difference here is that you use left_node and right_node instead of next_node).
 
</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2: [CODE]</b>&nbsp; Find a proper place for a node</summary><br>

The iterative solution for the sub-problem "how to find the proper place" is presented below:

```ruby
# the implementation may be slightly different
# focus on how it works
def insert(new_node)
  # use current as an iterator
  current = @root
  # it iterates over the tree until it's null
  until current.nil?
    # if the new node is less than current node, go to the left descendant
    if new_node.data <= current.data
      current = current.left
    # if the new node is greater than current node, go to the right descendant
    elsif new_node.data > current.data
      current = current.right
    end
  end
end
```
Now you just need to solve the second sub-problem: appending the new node to that place.

<b>`Hint:`&nbsp;You need to assign new_node to the parent element of the latest current (i.e. the null one).</b>

</details>
<!-- Hint 2 -->

---

<!-- Hint 3 -->
<details>
  <summary><b>Hint 3: [CODE]</b>&nbsp;Append the new node</summary><br>

```ruby
def insert(new_node)
  # use the parent variable for tracking the parent node through the iterations
  parent = nil
  current = @root
  until current.nil?
    # before we go into the descendant node, set parent equal to current
    parent = current
    if new_node.data <= current.data
      current = current.left
    elsif new_node.data > current.data
      current = current.right
    end
  end
end
```

Now, you already know which element is the parent of new_node. In the final step, you will need to assign new_node as a child of the parent node.

<b>`Hint:`&nbsp;Assign new_node to the parent element. Be careful--the parent node has two pointers: left_node and right_node. Which one should you use?</b><br>
<b>`Hint:`&nbsp;Keep in mind that the only element that can have a null parent is the root element.</b>

</details>
<!-- Hint 3 -->

---

<!-- Solution 1 -->
<details>
  <summary><b>[SPOILER ALERT!]&nbsp;</b>Naive solution (Iterative)</summary><br>

```ruby
# Node class implementation omitted
class BST
  # accessors and constructor omitted
  # pre_order method omitted

  def insert(new_node)
    parent = nil
    current = @root

    until current.nil?
      parent = current
      if new_node.data <= current.data
        current = current.left
      elsif new_node.data > current.data
        current = current.right
      end
    end
    # if the parent is nil, it means it's the root element
    if parent.nil?
      @root = new_node
    elsif new_node.data <= parent.data
      # append the new node to the left leaf
      parent.left = new_node
    elsif new_node.data > parent.data
      # append the new node to the right leaf
      parent.right = new_node
    end
  end
end
```

</details>
<!-- Solution 1 -->

---

<!-- Solution 2 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;More advanced solution, without using the parent variable (Iterative)</summary><br>

```ruby
class BST
  # accessors and constructor omitted
  # pre_order method omitted

  def insert new_node
    # if the tree is empty, set new_node as the root element
    # no need to iterate at all, then end the function call
    if @root.nil?
      @root = new_node
      return
    end
    # the iteration is same as in the previous solution
    current = @root
    until current.nil?
      if new_node.data < current.data
      # not using the parent variable
      # append the element and end the function call if the left leaf is nil
        if current.left.nil?
          current.left = new_node
          return
        end
        current = current.left
      elsif new_node.data > current.data
        # not using the parent variable
        # append the element and end the function call if the right leaf is nil
        if current.right.nil?
          current.right = new_node
          return
        end
        current = current.right
      end
    end
  end
end
```

</details>
<!-- Solution 2 -->

---

<!-- Solution 3 -->
<details>
  <summary><b>[SPOILER ALERT!]&nbsp;</b>Final solution (Recursive)</summary><br>

```ruby
# Node class implementation omitted

class BST
  # accessors and constructor omitted
  # pre_order method omitted

  # you need to assign the default starting point
  # in the recursive calls, you will pass the inner element as head
  def insert(new_node, head = @root)
    # base case: catch if the tree is empty
    if @root.nil?
      @root = new_node
      return
    end
    # it helps to break the recursion when you find the empty spot
    if head.nil?
      return new_node
    end
    # if the new node is less than the head, go to the left descendant
    if new_node.data < head.data
      head.left = insert(new_node, head.left)
    # if the new node is greater than the head, go to the right descendant
    elsif new_node.data > head.data
      head.right = insert(new_node, head.right)
    end
    # return the calculated head
    return head
  end
end
```

</details>
<!-- Solution 3 -->
