# Trees and Graphs: Is It a Search Tree?

<!-- Hint-1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

The goal of this challenge is to determine if a given tree follows the Binary Search Tree rules.

<b>`Hint:`&nbsp;Starting from the root node, traverse the tree. Stop at every node and check if the current node:<br>
&emsp;\- is less than its left subtree<br>
&emsp;\- is greater than its right subtree</b>


<b>`Note:`</b>&nbsp;Focus on the recursive solution. It is easier than the iterative one. Since our brains are not effective at solving recursive problems, write down the solution on a piece of paper first. [**This**](https://www.natashatherobot.com/recursion-factorials-fibonacci-ruby/) link will remind you how to solve recursive problems on paper.<br>

</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;How to check if a node is valid</summary><br>

Each internal node has two boundaries: the upper bound (the maximum) and the lower bound (the minimum).

Try to imagine a node placed at the position described as "root → left → right". The picture below illustrates this:<br>

&emsp;&emsp;&emsp;&emsp;A<br>
&emsp;&emsp;B&emsp;&emsp;&emsp;C<br>
&nbsp;D&emsp;&emsp;&nbsp;`E`

The node **"E"** must be greater than its parent (**"B"**), and **at the same time**, it must be less than its grandparent (**"A"**). This means:<br>
&emsp;**- The `minimum` value of E is (B + 1)**<br>
&emsp;**- The `maximum` value of E is  (A - 1).**

Take a look at the example with numbers:

 <img src="images/trees_and_graphs/is-it-a-search-tree/is-it-a-search-tree-1.png"  width="800">

<b>`Hint:`&nbsp;Use "min" and "max" values as the parameters of your recursive function.<br>
<b>`Hint:`&nbsp;Keep in mind that there is no constraint for the root node. It does not need to be greater than or less than anything. Leave "min" and "max" parameters empty for the root node.<br>
<b>`Hint:`&nbsp;When you traverse the left subtree, set the current node as the "max" of the next recursive call node.<br>
<b>`Hint:`&nbsp;When you traverse the right subtree, set the current node as the "min" of the next recursive call node.<br>
  
</details>
<!-- Hint 2 -->

---

<!-- Hint 3 -->
<details>
  <summary><b>Hint 3:</b>&nbsp;Structure of the recursive function</summary><br>

Since you are here, you should know how to traverse a tree. Here, we will focus on what we need to do at each step of traversing.

As a quick reminder, there are two main requirements of a recursive function:<br>
&emsp;\- A stop condition - the function returns a value when a certain condition is satisfied, **without a further recursive call.**<br>
&emsp;\- The recursive call - the function **calls itself**.

In this challenge, we will need two stop conditions:

<b>`Hint:`&nbsp;When a node is empty (nil), return "true" and the program continues traversing.<br>
<b>`Hint:`&nbsp;When a node is invalid, return "false"  and the program stops traversing.<br>
  
</details>
<!-- Hint 3 -->

---

<!-- Hint 4 -->
<details>
  <summary><b>Hint 4: [CODE]</b>&nbsp;Define the recursive calls</summary><br>

In the previous hint, we described the stop conditions. Here is a simple implementation of those conditions:

```ruby
def search? (tree)
  root = array_to_tree(tree)
  search_helper(root)
end

def search_helper(node, min, max)
  # an empty node means the current branch is valid
  return true if node.nil?
  # if there's a min constraint and the current node is less than that, then the current branch is invalid
  return false if min && node.data < min
  # if there's a max constraint and the current node is greater than that, then the current branch is invalid
  return false if max && node.data > max

  # the current node has been validated above.
  # the next step is to validate the left and right subtrees of the current node.

  # implement recursive calls in here!
end
```

Now, it is time to focus on implementing a recursive call (or calls). 

<b>`Hint:`&nbsp;You need to validate both the left and the right subtrees.<br>
<b>`Hint:`&nbsp;When you traverse the left subtree, set the current node as the "max" of the next recursive call node and use the current "min" as the "min" of the next recursive call node.<br>
<b>`Hint:`&nbsp;When you traverse the right subtree, set the current node as the "min" of the next recursive call node and use the current "max" as the "max" of the next recursive call node.<br>
  
</details>
<!-- Hint 4 -->

---

<!-- Solution 1 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution (Recursive)</summary><br>

```ruby
def search_tree? (tree)
  root = array_to_tree(tree)
  search_helper(root)
end

def search_helper (node, min = nil, max = nil)
  # an empty node means the current branch is valid
  return true if node.nil?
  # if there's a min constraint and the current node is less than that, then the current branch is invalid
  return false if min && node.data < min
  # if there's a max constraint and the current node is greater than that, then the current branch is invalid
  return false if max && node.data > max
  # recursive calls
  # the first one validates the left subtree 
  # the second one validates the right subtree
  search_helper(node.left, min, node.data) && search_helper(node.right, node.data, max)
end
```
  
</details>
<!-- Solution 1 -->

---

<!-- Solution 2 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp; Solution (Iterative) - only for the curious 😎</summary><br>

```ruby
def search_tree?(tree)
  root = array_to_tree(tree)
  stack = []
  current = root
  previous = nil

  while !stack.empty? || current
    # if the current node is not nil, then traverse its left subtree
    if current
      stack.push(current)
      # set the left subtree as current
      current = current.left
    # if the current node is nil, then traverse the right subtree of its parent
    else
      # get back the parent node from stack
      current = stack.pop
      return false if previous && current.data < previous.data

      previous = current
      # set the right subtree as current
      current = current.right
    end
  end
  # return true if it passes all nodes
  true
end
```
  
</details>
